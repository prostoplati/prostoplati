const React = require('react');

const cl = require('classnames');

class Header extends React.Component {
    render () {
        const navigation = this.props.navigation;

        return (
            <div className="header clearfix">
                <nav>
                    <ul className="nav nav-pills float-right">
                        {navigation.items.map(item => (
                            <li key={item.key} className="nav-item">
                                <a
                                    className={cl('nav-link', {active: item.key === navigation.activeKey})}
                                    href={item.path}
                                >{item.name}</a>
                            </li>
                        ))}
                    </ul>
                </nav>
                <h3 className="text-muted">ПростоПлати</h3>
            </div>
        );
    }
}

module.exports = Header;
