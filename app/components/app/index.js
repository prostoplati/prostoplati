const React = require('react');

const PageLayout = require('../page-layout');
const MainPage = require('../main-page');
const LoginPage = require('../login-page');
const ErrorPage = require('../error-page');
const AgentPage = require('../agent-page');

require('./index.scss');

class App extends React.Component {
    render () {
        const Page = {
            login: LoginPage,
            main: MainPage,
            error: ErrorPage,
            agent: AgentPage
        }[this.props.page.type];
        return (
            <PageLayout layout="container">
                <Page {...this.props}/>
            </PageLayout>
        );
    }
}

module.exports = App;
