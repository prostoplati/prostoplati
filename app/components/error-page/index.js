const React = require('react');

const Header = require('../header');

class MainPage extends React.Component {
    render () {
        return (
            <React.Fragment>
                <Header
                    navigation={this.props.navigation}
                />
                <h1>ErrorPage</h1>
                <p>Скорее всего это страница еще не сделана</p>
            </React.Fragment>
        );
    }
}

module.exports = MainPage;
