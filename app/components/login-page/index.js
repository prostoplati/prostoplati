const React = require('react');

const Header = require('../header');
const fetch = require('../../utils/fetch');

require('./index.scss');

class LoginPage extends React.Component {
    constructor (props) {
        super(props);
        this.state = {
            loading: false
        };
    }

    render () {
        return (
            <React.Fragment>
                <Header
                    navigation={this.props.navigation}
                />
                {this._renderForm()}
            </React.Fragment>
        );
    }

    _renderForm () {
        if (this.state.loading) {
            return this._renderLoading();
        }

        return (
            <form className="form-signin" onSubmit={event => this._onSubmit(event)}>
                <h1 className="h3 mb-3 font-weight-normal">Авторизация</h1>
                <input
                    ref={input => this._loginInput = input}
                    type="text"
                    id="inputEmail"
                    className="form-control"
                    placeholder="Логин"
                    defaultValue="qwerty121"
                    required=""
                    autoComplete="off"
                    autoFocus=""
                />
                <input
                    ref={input => this._passwordInput = input}
                    type="password"
                    id="inputPassword"
                    defaultValue="123"
                    className="form-control"
                    placeholder="Пароль"
                    required=""
                />
                <button className="btn btn-lg btn-primary btn-block" type="submit">Войти</button>
            </form>
        );
    }

    _renderLoading () {
        return (
            <p>Пожалуйста, подождите...</p>
        );
    }

    _onSubmit (event) {
        event.preventDefault();
        this.setState({loading: true});

        fetch('/api/login', {
            body: {
                login: this._loginInput.value,
                password: this._passwordInput.value
            },
            method: 'post'
        })
            .then(() => window.location.href = '/')
            .catch(error => null)
            .then(() => this.setState({loading: false}));
    }
}

module.exports = LoginPage;
