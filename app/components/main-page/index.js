const React = require('react');

const Header = require('../header');

class MainPage extends React.Component {
    render () {
        const subagents = this.props.auth.user.subagents;
        return (
            <React.Fragment>
                <Header
                    navigation={this.props.navigation}
                />
                <h1>Мои субагенты</h1>
                <h2>Мой баланс: {this.props.auth.user.balance}</h2>
                <ul>
                    {subagents.map(agent => (
                        <li key={agent}>
                            <a href={`/agent/${agent}`}>{agent}</a>
                        </li>
                    ))}
                </ul>
            </React.Fragment>
        );
    }
}

module.exports = MainPage;
