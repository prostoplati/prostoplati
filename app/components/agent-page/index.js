const React = require('react');

const Header = require('../header');

class AgentPage extends React.Component {
    render () {
        const subagents = this.props.auth.user.subagents;
        return (
            <React.Fragment>
                <Header
                    navigation={this.props.navigation}
                />
                <h1>Страница агента</h1>
                <h2>Баланс: {this.props.page.agent.balance}</h2>
                <ul>
                    {this.props.page.agent.subagents.map(agent => (
                        <li key={agent}>
                            <a href={`/agent/${agent}`}>{agent}</a>
                        </li>
                    ))}
                </ul>
            </React.Fragment>
        );
    }
}

module.exports = AgentPage;
