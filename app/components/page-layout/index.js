const React = require('react');
const cl = require('classnames');

require('./index.scss');

class PageLayout extends React.Component {
    render () {
        return (
            <div className={cl(this.props.layout, 'page-layout')}>
                {this.props.children || 'kek'}
            </div>
        );
    }
}

module.exports = PageLayout;
