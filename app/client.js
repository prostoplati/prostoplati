import React from 'react';
import ReactDOM from 'react-dom';
const App = require('./components/app');
console.log('client!');

document.addEventListener('DOMContentLoaded', () => {
    const state = JSON.parse(document.getElementById('state').innerText);

    ReactDOM.render(<App {...state} />, document.querySelector('[data-reactroot]'));
});
