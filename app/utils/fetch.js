require('whatwg-fetch');

const querystring = require('querystring');

module.exports = (path, options = {}) => {
    const headers = Object.assign(options.headers || {}, {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'X-Requested-With': 'XMLHttpRequest'
    });
    const method = options.method
        ? options.method.toUpperCase()
        : 'GET';

    const requestOptions = {
        method,
        headers,
        cache: 'default',
        body: options.body,
        credentials: 'include'
    };

    const {body, query = {}} = options;

    if (body) {
        requestOptions.body = JSON.stringify(body);
    }

    delete requestOptions.query;

    // eslint-disable-next-line
    return fetch(`${path}?${querystring.stringify(query)}`, requestOptions).then(response => {
        const contentType = response.headers.get('content-type');
        let isJson = true;

        if (contentType && contentType.indexOf('application/json') === -1) {
            isJson = false;
        }

        const dataPromise = isJson
            ? response.json()
            : response.text();

        return dataPromise.then(body => {
            if (response.ok) {
                return body;
            }

            const ex = new Error(response.statusText);
            ex.status = response.status;
            ex.body = body;
            throw ex;
        });
    });
};
