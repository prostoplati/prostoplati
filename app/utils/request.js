const got = require('got');

const REQUEST_TIMEOUT = 60000;
const MAX_RETRIES = 3;
// TODO mv to env variables
const BACKEND_BASE_URL = 'http://localhost:3001';

module.exports = (path, params = {}) => {
    const method = (params.method || '').toLowerCase() || 'get';
    const requestParams = {
        json: params.json,
        headers: params.headers || {},
        timeout: REQUEST_TIMEOUT,
        retries: count => count >= MAX_RETRIES ? 0 : 1
    };

    if (params.body) {
        requestParams.body = JSON.stringify(params.body);
        Object.assign(requestParams.headers, {
            'content-type': 'application/json',
            'accept': 'application/json'
        });
    }

    requestParams.query = params.query || {};
    requestParams.method = method;
    const url = `${BACKEND_BASE_URL}${path}`;
    requestParams.url = url;

    return got[method](url, requestParams);
};
