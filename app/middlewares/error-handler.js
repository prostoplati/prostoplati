module.exports = (error, req, res, next) => {
    req.state.page.error = error;
    next();
}
