const React = require('react');
const ReactDOM = require('react-dom/server');
const App = require('../../dist/index.js');

module.exports = (req, res, next) => {
    if (req.xhr) {
        res.status(404);
        res.json({error: 'not found'});
        return;
    }

    res.render('default', {
        title: 'ProstoPlati',
        state: req.state,
        html: ReactDOM.renderToString(
            React.createElement(App, req.state)
        )
    });
};
