const request = require('../utils/request');

module.exports = (req, res, next) => {
    request(`/api/agents/${req.params.agentId}`).then(res => {
        // TODO mv parsing to request
        const {agent} = JSON.parse(res.body);
        req.state.page.agent = agent;
        req.state.page.type = 'agent';
        next();
    }).catch(next);
};
