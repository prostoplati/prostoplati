const {
    LOGIN
} = require('../constants/navigation');

module.exports = (req, res, next) => {
    req.state.page.type = 'login';
    req.state.navigation.activeKey = LOGIN.key;
    next();
};
