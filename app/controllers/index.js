const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');

router.get('/', require('./main'));
router.get('/login', require('./login'));
router.get('/logout', require('./logout'));
router.get('/agent/:agentId', require('./agent'));
router.post('/api/login', bodyParser.json(), require('./api/login'));

module.exports = router;
