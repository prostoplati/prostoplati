module.exports = (req, res, next) => {
    res.cookie('auth', '');
    res.redirect('/');
};
