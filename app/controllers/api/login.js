const request = require('../../utils/request');

module.exports = (req, res) => {
    request('/api/authorize/', {
        body: {
            login: req.body.login,
            password: req.body.password
        },
        method: 'post'
    }).then(({body}) => {
        body = JSON.parse(body);
        const token = body.token;
        res.cookie('auth', token, {httpOnly: true});
        res.json({ok: true});
    }).catch(() => {
        res.status(401);
        res.json({error: 'invalid auth params'});
    });
};
