const {
    MAIN
} = require('../constants/navigation');

module.exports = (req, res, next) => {
    if (req.state.auth.authorized) {
        req.state.page.type = 'main';
        req.state.navigation.activeKey = MAIN.key;
        next();
    } else {
        res.redirect('/login');
    }
};
