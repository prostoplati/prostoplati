const express = require('express');
const serveStatic = require('serve-static');
const cookieParser = require('cookie-parser')
const request = require('./utils/request');

const {
    LOGIN,
    LOGOUT,
    REGISTER,
    MAIN
} = require('./constants/navigation');

const app = express();

app.set('views', './app/layouts')
app.set('view engine', 'pug')

app.disable('x-powered-by');
app.disable('etag');

const getAuth = req => {
    if (!req.cookies.auth) {
        return Promise.resolve({authorized: false});
    }

    return request('/api/user-by-token', {
        body: {
            token: req.cookies.auth
        },
        method: 'post'
    }).then(({body}) => {
        // TODO mv JSON.parse to request
        body = JSON.parse(body);
        const user = body.user;
        return {authorized: true, user};
    }).catch((er) => ({authorized: false}));
};

app.use('/dist', serveStatic('dist'));
app.use('/assets', serveStatic('assets'));

app.use(cookieParser());

app.use((req, res, next) => {
    getAuth(req).then(auth => {
        req.state = {
            page: {
                type: 'error'
            },
            auth,
            navigation: {
                items: [],
                active: null
            }
        };

        if (auth.authorized) {
            req.state.navigation.items.push(MAIN);
            req.state.navigation.items.push(LOGOUT);
        } else {
            req.state.navigation.items.push(LOGIN);
            req.state.navigation.items.push(REGISTER);
        }

        next();
    });
});

app.use(require('./controllers'));
app.use(require('./middlewares/error-handler'));
app.use(require('./middlewares/response'));

app.listen(3000, () => console.log('frontend started'));
