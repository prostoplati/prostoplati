module.exports = {
    LOGIN: {
        key: 'LOGIN',
        name: 'Войти',
        path: '/login'
    },

    REGISTER: {
        key: 'REGISTER',
        name: 'Зарегистрироваться',
        path: '/register'
    },

    MAIN: {
        key: 'MAIN',
        name: 'Главная',
        path: '/'
    },

    LOGOUT: {
        key: 'LOGOUT',
        name: 'Выйти',
        path: '/logout'
    }
};
