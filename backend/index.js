const express = require('express');
const serveStatic = require('serve-static');
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken')

const app = express();
const jsonParser = bodyParser.json();

app.disable('x-powered-by');
app.disable('etag');

const SECRET = 'supersecret';

const users = [
    {
        login: 'qwerty121',
        password: '123',
        subagents: ['agent1', 'agent2'],
        balance: 777,
    },

    {
        login: 'agent1',
        password: '123',
        subagents: ['agent3'],
        balance: 100,
    },
    {
        login: 'agent2',
        password: '123',
        subagents: [],
        balance: 300,
    },
    {
        login: 'agent3',
        password: '123',
        subagents: [],
        balance: 300,
    },
];

app.post('/api/authorize', jsonParser, (req, res, next) => {
    const user = users.find(
        x => x.login === req.body.login && x.password === req.body.password
    );

    if (!user) {
        res.status(401);
        res.json({error: 'bad credentials'});
        return;
    }

    const token = jwt.sign({user}, SECRET);

    res.json({token});
});

app.post('/api/user-by-token', jsonParser, (req, res, next) => {
    const token = req.body.token;
    let result = null;

    try {
        result = jwt.verify(token, SECRET);
    } catch (error) {
        console.log(error);
    }

    if (!result) {
        res.status(401);
        res.json({error: 'bad credentials'});
        return;
    }

    const user = result.user;

    res.json({user});
});

app.get('/api/agents/:agentId', (req, res) => {
    const user = users.find(user => user.login === req.params.agentId);
    if (!user) {
        res.status(404);
        res.json({error: 'not found'});
        return;
    }

    res.json({agent: user});
});

app.listen(3001, () => console.log('backend started'));
